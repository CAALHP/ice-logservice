﻿using System;
using System.Collections.Generic;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization.JSON;
using log4net;

namespace CAALHP.SOA.ICE.LogService
{
    public class LogImplementation : IServiceCAALHPContract
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public IServiceHostCAALHPContract Host { get; set; }
        private int _processId;
        private readonly List<string> _subscriptionList;

        public LogImplementation()
        {
            //Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            _subscriptionList = new List<string>();
            Log.Info("LogImplementation constructed");
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            Log.Info(notification.Key + " : " + notification.Value);
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            if (type != typeof(NewEventTypeAddedEvent)) return;
            var obj = JsonSerializer.DeserializeEvent(notification.Value, type) as NewEventTypeAddedEvent;
            if (obj == null) return;
            Log.Info("LogService subscribing to " + obj.EventType);
            Host.Host.SubscribeToEvents(obj.EventType, _processId);
            _subscriptionList.Add(notification.Key);
        }

        public string GetName()
        {
            return "LogService";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            foreach (var subscription in _subscriptionList)
            {
                Host.Host.UnSubscribeToEvents(subscription, _processId);
            }
            _subscriptionList.Clear();
            Environment.Exit(0);
        }

        public void Initialize(IServiceHostCAALHPContract hostObj, int processId)
        {
            Host = hostObj;
            _processId = processId;
            SubscribeToAllEvents();
            Log.Info("LogService Initialized");
            Console.WriteLine("LogService Initialized");
        }

        private void SubscribeToAllEvents()
        {
            Console.WriteLine("SubscribeToAllEvents...");
            var knownEvents = Host.GetListOfEventTypes();
            _subscriptionList.AddRange(knownEvents);
            foreach (var knownEvent in knownEvents)
            {
                Host.Host.SubscribeToEvents(knownEvent, _processId);
                Console.WriteLine("LogService subscribing to " + knownEvent);
                Log.Info("LogService subscribing to " + knownEvent);
            }
        }

        public void Start()
        {
            Log.Info("LogService started");
        }

        public void Stop()
        {
            Log.Info("LogService stopped");
            Environment.ExitCode = 0;
            Environment.Exit(Environment.ExitCode);
        }
    }
}